const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        let data = {};
        let attr = paramValue[0].replace('--', '');
        if (attr === 'id') {
            acc['id'] = paramValue[1];
        } else {
            data[attr] = paramValue[1];
            acc['data'] = data;
        }
    }
    return acc;
}, {});

function exec(id, data) {
    let index = 1;
    let setBlock = [], dataBlock = [];
    let q = 'UPDATE newposts SET ';
    for (let key in data) {
        setBlock.push(key + ' = $' + index + ' ');
        dataBlock.push(data[key]);
    }
    q += setBlock.join(',') + `WHERE id = ${id} RETURNING id;`;
    let result = connection.query(q, dataBlock);
    connection.end();
    return result;
}

exec(options.id, options.data)
    .then(result => console.log('Updated record with id: ', result.rows[0].id))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}