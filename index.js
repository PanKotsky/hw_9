const getAll = require('./getAll');
const getById = require('./getById');
const insert = require('./insert');
const updateById = require('./update');
const deleteById = require('./delete');

const operation = [
      'getAll',
      'getById',
      'insert',
      'updateById',
      'deleteById',
];

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        let data = {};
        let attr = paramValue[0].replace('--', '');

        if (operation.includes(attr)) {
            acc['func'] = attr;
        } else if (attr === 'id') {
            acc['id'] = paramValue[1];
        } else {
            data[attr] = paramValue[1];
            acc['data'] = data;
        }
    }
    return acc;
}, {});

const func = options.func;
const id = options.id;
const data = options.data;

let result;

switch (func) {
    case 'getAll':
        getAll.exec();
        break;
    case 'getById':
        getById.exec(id);
        break;
    case 'insert':
        insert.exec(data);
        break;
    case 'updateById':
        updateById.exec(id, data);
        break;
    case 'deletedById':
        deleteById.exec(id);
        break;
    default:
        throw new Error('The action doesn\'t allowed');
}