const Pool = require("pg").Pool;
const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "testdb",
    password: "postgres",
    port: "5439"
});

module.exports = pool;