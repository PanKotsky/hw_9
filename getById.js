const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(id) {
    let results = connection.query(
            `SELECT * 
             FROM newposts
             WHERE id = ${id};`);
    connection.end();
    return results;
}

exec(options.id).then(result => console.table(result.rows));


module.exports = {
    exec: exec
}