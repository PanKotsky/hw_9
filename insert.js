const connection = require('./connection');

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    let result = connection.query(
        `INSERT INTO newposts (title, text) VALUES ($1, $2) RETURNING id`, [data.title, data.text]);
    connection.end();
    return result;
}

exec(options)
    .then(result => console.log('Inserted record with id: ', result.rows[0].id))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}
