const connection = require('./connection');

connection.query(
    `CREATE TABLE newposts
     (
         id serial primary key,
         title text,
         text text,
         created_date date not null default CURRENT_DATE
     );`,
    (error, results) => {
        if (error) {
            throw error;
        }
        console.log(results);
    }
);
connection.end();