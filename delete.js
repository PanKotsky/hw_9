const connection = require("./connection");

const options = process.argv.reduce((acc, cur) => {
    if (cur.includes('--')) {
        const paramValue = cur.split('=');
        acc[paramValue[0].replace('--', '')] = paramValue[1];
    }
    return acc;
}, {});

function exec(data) {
    let result = connection.query(
        `DELETE FROM newposts 
         WHERE id = ${data.id};`);
    connection.end();
    return result;
}

exec(options)
    .then(result => console.log(result))
    .catch(err => console.error(err));

module.exports = {
    exec: exec
}