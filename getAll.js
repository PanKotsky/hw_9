const connection = require('./connection');

function exec() {
    let result = connection.query(
        `SELECT * 
         FROM newposts;`);
    connection.end();
    return result;
}

exec().then(result => console.table(result.rows));

module.exports = {
    exec: exec
}